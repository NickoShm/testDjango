FROM python:3.11.6

ENV PYTHONDONTWRITEBYTECODE 1
ENV PYTHONUNBUFFERED 1

RUN pip install --upgrade pip

WORKDIR /app

COPY . .

RUN pip install -r requirements.txt


CMD ["gunicorn", "-b", "0.0.0.0:8001", "myApp.wsgi:application"]